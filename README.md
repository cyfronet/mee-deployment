# MEE production deployment

Deployment scripts, files and instructions for MEE production machine.

## Setup

Deploying on a fresh machine requires you to run `setup.sh` script once to install needed dependencies
```
./setup.sh
```

You have to set up the repositories (these can be bare) with post-receive hooks and Gitlab CI/CD to push new versions to them. Currently these environments include dev and production.

Environment files were prepared for dev(`.env.dev`) and production(`.env.prod`) environments. You need to copy, rename them to `.env` and fill in secrets such as Gitlab tokens and copy SSH keys in the repository folders.
Production X509 private keys need to be generated for both environments and stored in `config/jwt/prod.pem` files.

The repository has config files already prepared for dev and production environments. You can, however, change them however you like after moving them into their place. Remember that changing variables in one file like port may require you to change it in another. Example of this is changing port in .env file for dev environment also requires change of port in master nginx service config file for dev.

Copy the `nginx-config` folder in this repository to your home folder. Any change to the configuration has to be done there now as this folder will be used as config repository for nginx.
To start Nginx, just run `docker-compose up -d` in the root of this repository.


You have to cd into dev or production environment folders before running commands below.

MEE deployments ran for the first time have to set up with:
```
docker-compose -f prod.docker-compose.yml run --rm web bin/setup
```

To setup sample data:
```
docker-compose -f prod.docker-compose.yml run --rm web rake primage:setup
```

You can then start the deployment with:
```
docker-compose -f prod.docker-compose.yml up -d
```

To turn off an environment run:
```
docker-compose -f prod.docker-compose.yml down
```

